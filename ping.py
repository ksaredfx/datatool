import socket
import optparse
import re
import time

parser = optparse.OptionParser()
parser.add_option("-i", "--host", type="string", dest="host", help="Server's Hostname or IP address", default='mc.ecocitycraft.com')
parser.add_option("-p", "--post", type="string", dest="port", help="Server's Port", default=25565)
parser.add_option("-l", "--loop",  action="store_true", dest="loop", help="If you want it to loop or not", default=False)
parser.add_option("-t", "--time",  type="int", dest="time", help="Seconds inbetween each loop, accepts decimals. Loops are usually 0.3s but can be up to 8", default=1)
parser.add_option("-q", "--quiet",  action="store_true", dest="quiet", help="Only print errors", default=False)
parser.add_option("-r", "--reduced",  action="store_true", dest="reduced", help="Show reduced information", default=False)
parser.add_option("-d", "--debug",  action="store_true", dest="debug", help="Quit on every error", default=False)
(options, args) = parser.parse_args()


def isOpen(ip,port,reduced):
    clock = time.time()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(8)
    try:
        s.connect((ip, int(port)))
    except:
        try:
            s.close()
        except:
            pass
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(8)
        s.connect((ip, int(port)))
    s.send('\xFE\x01')
    data = s.recv(4056)
    info = data[3:].decode("utf-16be").strip(u"\xa7").split(u"\x00")
    motd = re.sub(r'\xa7.?', '', info[3])
    s.close()
    length = time.time()-clock
    if reduced == True:
        return "Players: %s/%s - MOTD: %s - Version: %s - Time: %s" % (info[4],info[5],motd,info[2],length)
    else:
        return dict(result=True, typ='mcdict', protocol=info[1], version=info[2], motd=motd, players=int(info[4]), maxplayers=int(info[5]))

run = True

options.ip = socket.gethostbyname(options.host)

while run == True:
    if options.debug == False:
        if options.quiet == True:
            try:
                isOpen(options.ip, options.port, options.reduced)
            except:
                print "Server %s:%s appears to be down - Quiet" % (options.host, options.port)
        if options.quiet == False:
            try:
                print isOpen(options.ip, options.port, options.reduced)
            except:
                print "Server %s:%s appears to be down" % (options.host, options.port)
    else:
        print isOpen(options.ip, options.port, options.reduced)
    time.sleep(int(options.time))
    if options.loop == False:
        run = False