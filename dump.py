import socket
import optparse
import re
import time
import simplejson as json
 
 
################ Edit This Below ##############
 
# The path to where your json file goes
filex = "/home/multinode/Update/results.json"
# The path to where your player json is stored
jsons = "/home/multinode/Json/"
 
# Your hosts listing
hosts = {
        'letsplay.brokengear.net':'26666',
        'private.brokengear.net':'26667',
        'economy.brokengear.net':'26668',
        'creative.brokengear.net':'26669',
        'survival.brokengear.net':'26670',
         }
 
################ Do Not Touch Below ###########
 
data = {}
 
def jsonWrite(data,filex):
    data = json.dumps(data, sort_keys=True, indent=4)
    f = open(filex, 'w+')
    f.seek(0)
    f.write(data)
    f.truncate()
    f.close()
 
def getInfo(host,jsons):
    data = open(jsons+host+'.json', 'r')
    data = json.loads(data.read())
    try:
        return data
    except: 
        return "No Data"
 
def isOpen(host,port,jsons):
    ip = socket.gethostbyname(host)
    clock = time.time()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(4)
    try:
        s.connect((ip, int(port)))
    except:
        try:
            s.close()
        except:
            pass
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(8)
        s.connect((ip, int(port)))
    s.send('\xFE\x01')
    data = s.recv(4056)
    info = data[3:].decode("utf-16be").strip(u"\xa7").split(u"\x00")
    motd = re.sub(r'\xa7.?', '', info[3])
    s.close()
    length = time.time()-clock
    return dict(result=True, typ='mcdict', protocol=info[1], version=info[2], motd=motd, players=int(info[4]), maxplayers=int(info[5]), updated=time.time(), users=getInfo(host,jsons))
 
for x, y in hosts.items():
    var = "%s:%s" % (x,y)
    try:
        data[var] = isOpen(str(x),int(y),jsons)
    except:
        data[var] = dict(result=False, typ='mcdict', updated=time.time())
 
jsonWrite(data,filex)
