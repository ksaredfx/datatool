import redis
import socket
import sys
import optparse
import time
import re

# Set the right timer depending on OS, May change in the future
timer = time.clock if sys.platform == 'win32' else time.time


parser = optparse.OptionParser()
runtime = optparse.OptionGroup(parser, "Runtime Settings",
                    "Settings to change how the script runs. ")
runtime.add_option("-s", "--setup", dest="setup", action="store_true", help="Run the setup and insert default data into the Redis Database", default=False)
runtime.add_option("-t", "--loop", type="string", dest="num", help="The number of times to loop the script", default='inf')
runtime.add_option("-x", "--stop", action="store_true", dest="noping", help="Disable the Main loop and only do not query servers, Use this if you wish to only update the database or check something and not run the loop", default=False)

parser.add_option_group(runtime)

database = optparse.OptionGroup(parser, "Database Settings",
                    "These settings are for changing how the script connects to the database. ")
database.add_option("-a", "--addr", type="string", dest="dba", help="Database Address, Default Localhost", default='localhost')
database.add_option("-p", "--port", type="int", dest="dbp", help="Database Port, Default 6379", default=6379)
database.add_option("-n", "--num", type="int", dest="dbn", help="Database Number, Default 4", default=4)
parser.add_option_group(database)
query = optparse.OptionGroup(parser, "Database Query and Inserts",
                    "Options that allow you to query a database or insert data. ")
parser.add_option_group(query)
query.add_option("-i", "--insert", type="string", dest="new", help="Add a new server to the 'Servers' Database\nFormat: ipaddress:port\nExample: 127.0.0.1:80", default=0)
query.add_option("-l", "--list", dest="list", action="store_true", help="Get a list of all servers in the database", default=0)
query.add_option("-q", "--query", dest="query", type="string", help="Search for a specific server in the database", default=0)

(options, args) = parser.parse_args()

# Basic code to check if a port is open, and if a certain type of port, send special data
# If inside MC Port range, hit it with a HELLO MINECRAFT Packet of \xfe, else just gather the data possible
def isOpen(ip,port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
    sent = 'ERR'
    try:
        start = timer()
        s.connect((ip, int(port)))
        if (port >= 25000) and (port <= 27000):
            sent = 'XFE'
            s.send('\xFE\x01')
        else:
            sent = 'Hello'
            s.send('Hello')
        data = s.recv(4056)
        xtime = timer()-start
        if data[0] == "\xff" : # Check if Minecraft Data
            info = data[3:].decode("utf-16be").strip(u"\xa7").split(u"\x00")
            try: # Try to return parsed data
                motd = re.sub(r'\xa7.?', '', info[3])
                return dict(result=True, typ='mcdict', ping=xtime, protocol=info[1], version=info[2], motd=motd, players=int(info[4]), maxplayers=int(info[5]), str=sent)
            except: # Return a dump of the Minecraft Data if unparsable
                return dict(result=True, typ='mchunk', ping=xtime, dump=info, str=sent)
        else: # Not minecraft data
           return dict(result=True, typ='basic', ping=xtime, dump=repr(data), str=sent)
    except Exception,e: # Something went wrong in the connection, Dump error report
        return dict(result=False, typ='fail', err=str(e), str=sent)
    s.close()


# Basic Database Template
def Setup():
    r4.hset('servers','184.164.131.179',80)
    r4.hset('servers','mc.ecocitycraft.com',25565)
    r4.hset('servers','184.164.131.180',25561)
    r4.hset('servers','184.164.131.181',25566)
    r4.hset('servers','184.164.131.178',443)
    print "Set up a default redis hash"


# Setting up database connection
r4 = redis.StrictRedis(host=options.dba, port=options.dbp, db=options.dbn)

# Query'ing the database for basic info
servers = r4.hgetall('servers')
servers = {server: int(port) for server, port in servers.items()}

# If List is called, Print list FIRST
if options.list == True:
    for server, port in servers.items():
        print "IP %s + Port %s" % (server, port)

# If Setup Requested
if options.setup == True:
    print Setup()

# If Query Requested
if options.query != 0:
    if ':' in options.query:
        query_data = options.query.split(":", 1)
        if query_data[0] == '': # IP is blank so check by port
            for server, port in servers.items():
                if port == int(query_data[1]):
                    print "Matching Results By Port are: "
                    print "Server is %s and port is %s" % (server, port)
        else: # IP Wasn't blank and there was a : so check by both
            for server, port in servers.items():
                if server == query_data[0] and port == int(query_data[1]):
                    print "Matching Results By Server and Port are: "
                    print "Server is %s and port is %s" % (server, port)
    
    else: # There was no : in options.query so check by IP
        for server, port in servers.items():
            if server == options.query:
                print "Matching Results By Server are: "
                print "Server is %s and port is %s" % (server, port)

# If Insert Requested
if options.new != 0:
    if ':' in options.new:
        new_data = options.new.split(":", 1)
        print "Inserting into Redis Database IP as %s and Port as %s" % (new_data[0], new_data[1])
        r4.hset('servers', new_data[0], new_data[1])
    else:
        print "Didn't input data in the correct format"

# Main Loop
# If noping is not set, continue
if options.noping != True:
    if options.num == 'inf':
        Loop = 1
        print "Looping forever"
    else:
        Loop = int(options.num)
        print "Picked a loop number"
        print Loop

    while Loop != 0:
        for server, port in servers.items():
            print "Server IP is %s and port is %s" % (server, port)
            x = isOpen(server,port) 
            clock = (repr(timer())) 
            x = (repr(x)) #
            addr = "%s:%s" % (server, port) 
            r4.hset(addr, clock, x) 
        if options.num != 'inf':
            Loop = int(Loop) - 1
else:
    print "Main loop set to not run, exiting"
    quit()
print "Done"
