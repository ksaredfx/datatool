from util import http
import simplejson as json
import time
import urllib

plugin_json = 'plugins.json'
savedir = 'Update-Sc'

def parseList(plugin_json):
    try:
        plugin_list = open(plugin_json, 'r+')
        plugin_list = json.load(plugin_list)
    except: 
        print "No File - Creating"
        plugin_list = open(plugin_json, 'w')
        plugin_list.close()
        plugin_list = {}
        print "Your Config is incomplete"
        return
    latest_mc = plugin_list['MCVersion']
    plugins = plugin_list['Plugins']
    for plugin in plugins:
        home = plugins[plugin]['Home']
        date = plugins[plugin]['Date']
        getLatest(plugin, home, latest_mc, date)

def getLatest(plugin, home, latest_mc, last_update):
    if "dev.bukkit" in home.lower():
        try:
            thread = http.get_html("%s" % home)
        except:
            print "Couldn't get required HTML Data - Try again later!"
            return
        xdate = thread.xpath("//tr//td//span[@class='standard-date']/text()")
        newdate = xdate[0]
        pattern = '%b %d, %Y'
        newdate = int(time.mktime(time.strptime(newdate, pattern)))
        if newdate > last_update:
            xmcvers = thread.xpath("//tr//td//ul//li/text()")
            try:
                mcvers = float(".".join(xmcvers[0].split(".")[:2]))
            except:
                try:
                    mcvers = float(".".join(xmcvers[0].split(" ")[1].split("-")[0].split(".")[:2]))
                except:
                    print "Broke it all at MCVERS"
                    print xmcvers[0]
                    return
            if mcvers <= latest_mc:
                newlink = thread.xpath("//tr//td//a/@href")
                xnewvers = thread.xpath("//tr//td//a/text()")
                newvers = xnewvers[0]
                newlink = newlink[0]
                newlink = "http://dev.bukkit.org%s" % newlink
                downloadPage(plugin, newlink, newdate, newvers)
        else:
            print "%s already up to date" % plugin

def downloadPage(plugin, url, newdate, newvers):
    if "dev.bukkit" in url.lower():
        try:
            thread = http.get_html("%s" % url)
        except:
            print "Couldn't get required Inner Link Data - Try again later!"
            return
        link = thread.xpath("//li//span//a/@href")
        link = link[0]
        saveas = link.split("/")[-1]
    elif ""

    downloadUpdate(plugin, link, saveas, newdate, newvers)


def downloadUpdate(plugin, link, saveas, newdate, newvers):
    saveto = "./%s/%s" % (savedir, saveas)
    try:
        urllib.urlretrieve(link, saveto)
    except:
        print "Website down or Permission Error - Try again later!"
        return
    print "Downloaded new %s - Saved to %s - Version %s" % (plugin, saveto, newvers)
    updateList(plugin, newdate, newvers, link)

def updateList(plugin, newdate, newvers, url):
    plugin_list = open(plugin_json, 'r+')
    plugin_list = json.load(plugin_list)
    plugin_list['Plugins'][plugin]['Date'] = newdate
    plugin_list['Plugins'][plugin]['FileName'] = newvers
    plugin_list['Plugins'][plugin]['DownloadLink'] = url 
    f = open(plugin_json, 'w+')
    f.seek(0)
    json_seen = json.dumps(plugin_list, sort_keys=True, indent=4)
    f.write(json_seen)
    f.truncate()
    f.close()

parseList(plugin_json)