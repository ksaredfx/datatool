import DNS


def srvData(domain):
    DNS.ParseResolvConf()
    srv_req = DNS.Request(qtype = 'srv')
    srv_result = srv_req.req('_minecraft._tcp.%s' % domain)

    for getsrv in srv_result.answers:
        if getsrv['typename'] == 'SRV':
            data = [getsrv['data'][2],getsrv['data'][3]]
            return data

listeddata = srvData('brokengear.net')
print listeddata[0]
print listeddata[1]