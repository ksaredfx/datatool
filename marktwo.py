from util import http
import simplejson as json
import time
import urllib
import os

class ParseList(object):
    def __init__(self):
        self.config = 'marktwo.json'
        self.folder = 'Update-Sc2'

        try:
            os.mkdir(self.folder)
        except:
            pass

        self.core()

    def core(self):
        self.parseJson()
        plugins = self.plugins['Plugins']
        self.mcvers = float(self.plugins['MCVersion'])
        for plugin in plugins:
            self.setValues(plugin, plugins)
            if "dev.bukkit" in self.home:
                downloadpage = self.devBukkit()
                if downloadpage != "NONE":
                    saveas = self.devBukkitDownloads(downloadpage)
            elif "spigotmc.org" in self.home:
                self.spigotMC()
            else:
                self.jenkins()
            if downloadpage != "NONE":
                self.downloadPlugin(saveas)
                self.updateList()

    def parseJson(self):
        try:
            self.plugins = open(self.config, 'r+')
            self.plugins = json.load(self.plugins)
        except Exception as e:
            print "Config error: %s" % e
            pluginsfile = open(self.config, 'w')
            self.plugins = {}
            while self.plugins == {}:
                try:
                    self.plugins['MCVersion'] = float(raw_input("Current version of MC to a single decimal (Eg: 1.5 instead of 1.5.2) \n> "))
                except:
                    time.sleep(0.1)
                    pass
            self.plugins['Plugins'] = {}
            self.plugins['Plugins']['Vault'] = dict(Date=1, FileName="", Home="http://dev.bukkit.org/bukkit-plugins/vault/files/", Dev=False, Beta=False, Match="legacy", Direct="")
            json.dump(self.plugins, pluginsfile, sort_keys=True, indent=4)
            print "Completed initial config setup, Please edit the file as you wish"
            return

    def setValues(self, plugin, plugins):
        self.home = plugins[plugin]['Home']
        self.date = int(plugins[plugin]['Date'])
        self.match = plugins[plugin]['Match']
        self.beta = plugins[plugin]['Beta']
        self.dev = plugins[plugin]['Dev']
        self.plugin = plugin

    def devBukkit(self):
        try:
            thread = http.get_html("%sfiles/" % self.home)
        except:
            print "Couldn't get required HTML Data - Try again later!"
            return
        cur = 0
        versions = thread.xpath("//tr//td[@class='col-game-version']//ul//li[1]/text()")
        dates = thread.xpath("//tr//td//span[@class='standard-date']/text()")
        while cur < len(versions):
            self.epoch = int(time.mktime(time.strptime(dates[cur], '%b %d, %Y')))
            if self.epoch > self.date:
                try:
                    version = float(".".join(versions[cur].split(".")[:2]))
                except:
                    try:
                        version = float(".".join(versions[cur].split(" ")[1].split("-")[0].split(".")[:2]))
                    except:
                        print "Broke it all while checking version"
                        print versions[cur]
                        return
                if self.match.lower() == "legacy":
                    if version <= self.mcvers:
                        newlink = thread.xpath("//tr//td[@class='col-file']//a/@href")
                        releasetype = thread.xpath("//tr//td[@class='col-type']//span/text()")
                        if (self.dev == True) and (releasetype[cur].lower() == "alpha"):
                            pluginversions = thread.xpath("//tr//td[@class='col-file']//a/text()")
                            self.pluginversion = pluginversions[cur]
                            self.newmcversion = version
                            self.cur = cur
                            newlink = newlink[cur]
                            downloadpage = "http://dev.bukkit.org%s" % newlink
                            return downloadpage
                        elif (self.beta == True) and (releasetype[cur].lower() == "beta"):
                            pluginversions = thread.xpath("//tr//td[@class='col-file']//a/text()")
                            self.pluginversion = pluginversions[cur]
                            self.newmcversion = version
                            self.cur = cur
                            newlink = newlink[cur]
                            downloadpage = "http://dev.bukkit.org%s" % newlink
                            return downloadpage
                        elif (self.dev == False) and (self.beta == False) and (releasetype[cur].lower() == "release"):
                            pluginversions = thread.xpath("//tr//td[@class='col-file']//a/text()")
                            self.pluginversion = pluginversions[cur]
                            self.newmcversion = version
                            self.cur = cur
                            newlink = newlink[cur]
                            downloadpage = "http://dev.bukkit.org%s" % newlink
                            return downloadpage
                        else:
                            cur = cur + 1
                    else:
                        cur = cur + 1
                elif self.match.lower() == "newer":
                    if version > self.mcvers:
                        newlink = thread.xpath("//tr//td[@class='col-file']//a/@href")
                        releasetype = thread.xpath("//tr//td[@class='col-type']//span/text()")
                        if (self.dev == True) and (releasetype[cur].lower() == "alpha"):
                            pluginversions = thread.xpath("//tr//td[@class='col-file']//a/text()")
                            self.pluginversion = pluginversions[cur]
                            self.newmcversion = version
                            self.cur = cur
                            newlink = newlink[cur]
                            downloadpage = "http://dev.bukkit.org%s" % newlink
                            return downloadpage
                        elif (self.beta == True) and (releasetype[cur].lower() == "beta"):
                            pluginversions = thread.xpath("//tr//td[@class='col-file']//a/text()")
                            self.pluginversion = pluginversions[cur]
                            self.newmcversion = version
                            self.cur = cur
                            newlink = newlink[cur]
                            downloadpage = "http://dev.bukkit.org%s" % newlink
                            return downloadpage
                        elif (self.dev == False) and (self.beta == False) and (releasetype[cur].lower() == "release"):
                            pluginversions = thread.xpath("//tr//td[@class='col-file']//a/text()")
                            self.pluginversion = pluginversions[cur]
                            self.newmcversion = version
                            self.cur = cur
                            newlink = newlink[cur]
                            downloadpage = "http://dev.bukkit.org%s" % newlink
                            return downloadpage
                        else:
                            cur = cur + 1
                    else:
                        cur = cur + 1
                else:
                    cur = 99
                    print "No updates/matches avaliable for %s" % self.plugin
                    return "NONE"
            else:
                print "%s is already up to date" % self.plugin
                return "NONE"

    def devBukkitDownloads(self, downloadpage):
        try:
            thread = http.get_html("%s" % downloadpage)
        except:
            print "Couldn't get required Inner Link Data - Try again later!"
            return
        link = thread.xpath("//li//span//a/@href")
        self.filelocation = link[0]
        return link[0].split("/")[-1]

    def downloadPlugin(self, saveas):
        saveto = "./%s/%s" % (self.folder, saveas)
        try:
            urllib.urlretrieve(self.filelocation, saveto)
        except:
            print "Website down or Permission Error - Try again later!"
            return
        if self.cur == 0:
            cur = "Most recent version"
        else: 
            cur = "%s Versions out" % (self.cur+1)
        print "Downloaded new %s - %s - Saved to %s - %s - Minecraft %s" % (self.plugin, self.pluginversion, saveto, cur, self.newmcversion)

    def updateList(self):

        self.plugins['Plugins'][self.plugin]['Date'] = self.epoch
        self.plugins['Plugins'][self.plugin]['FileName'] = self.pluginversion
        self.plugins['Plugins'][self.plugin]['Direct'] = self.filelocation
        self.plugins['Plugins'][self.plugin]['MCVers'] = self.newmcversion
        
        f = open(self.config, 'w+')
        f.seek(0)
        json_seen = json.dumps(self.plugins, sort_keys=True, indent=4)
        f.write(json_seen)
        f.truncate()
        f.close()

ParseList()